# Minesweeper


## Installation
```shell
poetry install
```
or

```shell
python -m pip install -r requirements.txt
```

## How to run?
```shell
python main.py
```


## Why FastAPI?
- Swagger documentation
- Async/await support

## Where is the Docs?
```shell
http://localhost:8000/docs
```

## What's working?
- Load bomb grid
- Check cell:
  1. If the cell being checked corresponds to a bomb in the bombGrid, then end the game
  2. Check all neighboring cells surrounding the checked cell and count the number of them
  that are bombs according to the bombGrid. Replace the value of the cell on the gameGrid
  with that bomb count.
     
## What's missing?
- Check cell:
  3. If none of the neighbors are bombs and the gameGrid cell is replaced with '0', then
  all of the neighbors of the checked cell should also be checked (ad infinitum) (see
  `game.checkCell(0, 0)` below).

