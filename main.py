from typing import List, Tuple
from contextlib import suppress
import uvicorn
from pydantic import BaseModel
from fastapi import FastAPI, HTTPException

app = FastAPI()

bomb_grid = []
game_grid = []


@app.post("/load-bomb-grid")
async def load_bomb_grid(grid: List[List[int]]):
    bomb_grid.extend(grid)
    game_grid.extend([["_" for x in range(len(grid))] for y in range(len(grid[0]))])
    return game_grid


@app.post("/check-cell")
async def check_cell(col: int, row: int):
    if not bomb_grid:
        raise HTTPException(422, "Bomb grid not loaded yet.")
    if col > len(bomb_grid) or row > len(bomb_grid):
        raise HTTPException(422, "Invalid position")
    if is_bomb(col, row):
        return end_game()
    bomb_count = count_neighbors(col, row)
    game_grid[col][row] = bomb_count if bomb_count > 0 else 0

    return game_grid


def is_bomb(col, row) -> bool:
    with suppress(IndexError):
        return bomb_grid[col][row] == 1


def count_neighbors(col, row):
    counter = 0
    positions = [
        (row - 1, col - 1),
        (row, col - 1),
        (row + 1, col - 1),
        (row - 1, col),
        (row + 1, col),
        (row - 1, col + 1),
        (row, col + 1),
        (row + 1, col + 1),
    ]
    for x, y in positions:
        if x >= 0 and y >= 0:
            with suppress(IndexError):
                counter += bomb_grid[y][x] == 1

    return counter


def end_game():
    bomb_grid.clear()
    game_grid.clear()
    return "End game"


if __name__ == '__main__':
    uvicorn.run("main:app", reload=True)
